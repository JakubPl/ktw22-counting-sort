import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] sortMe = {1, 7, 1, 1, 8, 9, 5, 9};
        int[] countArray = new int[10];

        /*for(int i=0; i < sortMe.length; i ++) {
            int digit = sortMe[i];
            countArray[digit] ++;
        }*/

        for (int digit : sortMe) {
            countArray[digit]++;
        }
        System.out.println(Arrays.toString(countArray));

        //[0, 3, 0, 0, 0, 1, 0, 1, 1, 2]
        for (int i = 0; i < countArray.length; i++) {
            for (int j = 0; j < countArray[i]; j++) {
                System.out.println(i);
            }
        }
    }
}
